<?php

return [
    '[admin]' => [
        'Login/index' => [
            'admin/Login/index',
            ['method' => 'post']
        ],
        '__miss__'      => ['admin/Miss/index'],
    ],
];
